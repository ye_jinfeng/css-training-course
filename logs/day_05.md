## 内容
### 环境属性Environment
- @Profile
- Spring的Environment抽象提供了对可配置的属性源（PropertySource）层次结构的搜索操作
- PropertySource对象，Spring StandardEnvironment 配置有两个PropertySource对象：
    - 表示JVM系统属性集（System.getProperties()）
    - 表示系统环境变量集（System.getenv()）
```java
ApplicationContext ctx = new GenericApplicationContext();
Environment env = ctx.getEnvironment();
boolean containsMyProperty = env.containsProperty("my-property");
System.out.println("Does my environment contain the 'my-property' property? " + containsMyProperty);
```
- 通过编码方式，实例化您自己的PropertySource并将其添加到PropertySources当前的集合中Environment
```java
ConfigurableApplicationContext ctx = new GenericApplicationContext();
MutablePropertySources sources = ctx.getEnvironment().getPropertySources();
sources.addFirst(new MyPropertySource());
```
- 运用@PropertySource。该@PropertySource 注解提供便利和声明的机制添加PropertySource 到Spring的Environment。
- @PropertySource注解是可重复注解的（在一个类上面可添加多个）
```java
@Configuration
@PropertySource("classpath:/com/myco/app.properties")
public class AppConfig {

    @Autowired
    Environment env;

    @Bean
    public TestBean testBean() {
        TestBean testBean = new TestBean();
        testBean.setName(env.getProperty("testbean.name"));
        return testBean;
    }
}
```
- 资源位置中${…​}存在的任何占位符@PropertySource都是针对已针对环境注册的属性源集合进行解析的。例如：
- 假设它my.placeholder已存在于已注册的其中一个属性源中（例如，系统属性或环境变量），则占位符将解析为相应的值。如果没有，则default/path用作默认值。如果未指定默认值且无法解析属性， IllegalArgumentException则抛出a。
```java
@Configuration
@PropertySource("classpath:/com/${my.placeholder:default/path}/app.properties")
public class AppConfig {

    @Autowired
    Environment env;

    @Bean
    public TestBean testBean() {
        TestBean testBean = new TestBean();
        testBean.setName(env.getProperty("testbean.name"));
        return testBean;
    }
}
```
### ApplicationContext的附加功能
- 通过ResourceLoader界面访问URL和文件等资源。
- 事件发布，即ApplicationListener通过使用接口实现接口的bean ApplicationEventPublisher。
        - 从Spring 4.2开始，您可以使用EventListener注释在托管bean的任何公共方法上注册事件侦听器。
        - 基于注释的事件监听器: @EventListener
            - @EventListener({ContextStartedEvent.class, ContextRefreshedEvent.class})
            - @EventListener(condition = "#blEvent.content == 'my-event'")
        - [例子](https://docs.qq.com/doc/DUkJHcVdCVUNzZGVo)
        - 如果需要在另一个侦听器之前调用一个侦听器，则可以将@Order 注释添加到方法声明中:@Order(42)
- 方便的Web应用程序的ApplicationContext实例化。
```java
@EventListener(condition = "#blEvent.content == 'my-event'")
public void processBlackListEvent(BlackListEvent blEvent) {
    // notify appropriate parties via notificationAddress...
}
```
- [方便地访问低层资源](https://docs.spring.io/spring/docs/5.1.9.RELEASE/spring-framework-reference/core.html#context-functionality-resources)
    - 应用程序上下文是含有一个ResourceLoader，可用于加载Resource对象。
    - ResourceLoaderAware在初始化时自动回调。
- 方便的Web应用程序的ApplicationContext实例化
    - 您可以ApplicationContext使用ContextLoaderListener，注册一个。
    - 在web.xml里加下面代码的配置：
```xml
<context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>/WEB-INF/daoContext.xml /WEB-INF/applicationContext.xml</param-value>
</context-param>

<listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
</listener>
```
### 资源Resource
- 资源接口Resource
- [内置资源实现](https://docs.spring.io/spring/docs/5.1.9.RELEASE/spring-framework-reference/core.html#resources-implementations)
    - UrlResource
    - ClassPathResource
    - FileSystemResource
    - ServletContextResource
    - InputStreamResource
    - ByteArrayResource
```java
public interface Resource extends InputStreamSource {

    boolean exists();

    /*
        返回一个boolean指示此资源是否表示具有打开流的句柄的指示符。
        如果true，InputStream不能多次读取，必须只读一次然后关闭以避免资源泄漏。
        false是所有常规资源实现的返回值，但除了InputStreamResource。
    */
    boolean isOpen();

    URL getURL() throws IOException;

    File getFile() throws IOException;

    Resource createRelative(String relativePath) throws IOException;

    String getFilename();

    String getDescription();

}
```
- ResourceLoader接口
```java
public interface ResourceLoader {

    Resource getResource(String location);

}
```
- ApplicationContext已经实现了ResourceLoaderAware接口和ResourceLoader接口
```java
// 根据ApplicationContext的类型，返回Resource的类型
Resource template = ctx.getResource("some/resource/path/myTemplate.txt");
Resource template = ctx.getResource("classpath:some/resource/path/myTemplate.txt");// 返回ClassPathResource
Resource template = ctx.getResource("file:///some/resource/path/myTemplate.txt");
Resource template = ctx.getResource("https://myhost.com/resource/path/myTemplate.txt");
```