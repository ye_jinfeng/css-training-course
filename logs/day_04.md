## 主题仓库: [https://gitee.com/dgut-sai/spring-bean-demo](https://gitee.com/dgut-sai/spring-bean-demo)

## 内容:
### Spring 概述
### Spring IoC 容器
#### 1. Spring IoC容器和Bean简介
#### 2. 容器概述
- 实例化容器
- 使用容器
### Bean的概述
- Bean的命名
- 实例化Bean
### 依赖
- 依赖注入
- 
### Bean的范围
- 单例Bean
- 原型Bean
### 自定义bean特性的接口
- 生命周期回调
    - InitializingBean和DisposableBean接口的使用
- ApplicationContextAware 和 BeanNameAware
- [其他Aware接口](https://docs.spring.io/spring/docs/5.1.9.RELEASE/spring-framework-reference/core.html#aware-list)
### 容器扩展点
- BeanPostProcessor
- BeanFactoryPostProcessor
- 使用FactoryBean自定义实例化逻辑
### 基于注解的容器配置
- 注解是否比配置Spring的XML更好？
- 在隐式注册后处理器包括 AutowiredAnnotationBeanPostProcessor，CommonAnnotationBeanPostProcessor，PersistenceAnnotationBeanPostProcessor，RequiredAnnotationBeanPostProcessor。
- @Required注解适用于bean属性setter方法
- @Autowired注解应用于构造函数
- 微调基于注解的自动装配@Primary
- 使用限定符微调基于注解的自动装配@Qualifier
- CustomAutowireConfigurer 是一个BeanFactoryPostProcessor允许您注册自己的自定义限定符注解类型的
- @Resource、@PostConstruct、@PreDestroy的作用，注解解释器CommonAnnotationBeanPostProcessor
### 类路径扫描和@Component组件
- 原来是在xml文件中声明Bean
- 通过扫描类路径隐式检测候选组件的选项。候选组件是与筛选条件匹配的类，并且具有向容器注册的相应bean定义。
- Spring提供进一步典型化注解：@Component，@Service，和 @Controller。@Component是任何Spring管理组件的通用 构造型。
- Spring可以自动检测构造型类并注册相应的 BeanDefinition实例 到 ApplicationContext，最后 ApplicationContext 根据各个BeanDefinition实例 实例化 Bean。
- 组合注解
```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component 
public @interface Service {
    // ....
}
```
### 基于Java的容器配置
- 基本概念：@Bean和@Configuration
- 使用AnnotationConfigApplicationContext实例化Spring容器
- 使用@Bean注解
- 使用@Configuration注解
- 编写基于Java的配置
    - 使用@Import，导入其它配置类
- 有条件地包含@Configuration类或@Bean方法
    - 在大多数实际情况中，bean跨配置类彼此依赖。
    - 解决这个问题很简单，一个@Bean方法可以有任意数量的参数来描述bean的依赖关系。[例子](https://docs.spring.io/spring/docs/5.1.9.RELEASE/spring-framework-reference/core.html#beans-java-injecting-imported-beans)
    - 使用@Configuration类时，Java编译器会对配置模型施加约束，因为对其他bean的引用必须是有效的Java语法。
    - @Configuration仅在Spring Framework 4.3中支持类中的 构造函数注入。
    - 如果要影响某些bean的启动创建顺序，可以考虑将它们中的一些声明为@Lazy（用于在第一次访问时创建而不是在启动时）或@DependsOn某些其他bean（确保在当前bean之前创建特定的其他bean）
    - 下面的例子，是一个很好例子,通过配置类实现接口，达到解耦：
```java
@Configuration
public class ServiceConfig {

    @Autowired
    private RepositoryConfig repositoryConfig;

    @Bean
    public TransferService transferService() {
        return new TransferServiceImpl(repositoryConfig.accountRepository());
    }
}

@Configuration
public interface RepositoryConfig {

    @Bean
    AccountRepository accountRepository();
}

@Configuration
public class DefaultRepositoryConfig implements RepositoryConfig {

    @Bean
    public AccountRepository accountRepository() {
        return new JdbcAccountRepository();
    }
}

@Configuration
@Import({ServiceConfig.class, DefaultRepositoryConfig.class})  // import the concrete config!
public class SystemTestConfig {

    @Bean
    public DataSource dataSource() {
        // return DataSource
    }

}

public class test{
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SystemTestConfig.class);
        TransferService transferService = ctx.getBean(TransferService.class);
        transferService.transfer(100.00, "A123", "C456");
    }
}
```
- @Conditional与Condition接口
- @Profile的Condition接口实现，@Conditional(ProfileCondition.class)：
```java
@Override
public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
    if (context.getEnvironment() != null) {
        // Read the @Profile annotation attributes
        MultiValueMap<String, Object> attrs = metadata.getAllAnnotationAttributes(Profile.class.getName());
        if (attrs != null) {
            for (Object value : attrs.get("value")) {
                if (context.getEnvironment().acceptsProfiles(((String[]) value))) {
                    return true;
                }
            }
            return false;
        }
    }
    return true;
}
```

    