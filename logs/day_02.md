### 内容：
#### Maven教程
1. Maven核心概念
2. 配置Maven的中央仓库镜像（阿里云镜像）
3. Maven的pom.xml详细解释
4. Maven的插件使用
5. 使用Maven构建项目
6. Maven的依赖传递
7. Maven的Idea的操作
8. Maven的生命周期详解



#### 作业：
1. 使用命令行或idea工具，快速创建一个WebApp Maven项目.
2. 创建一个多模块Maven项目，掌握全项目编译、打包；掌握单项目编译、打包。